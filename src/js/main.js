require('../scss/app.scss');
require('../scss/swiper.min.css');
require('bootstrap/dist/css/bootstrap.css');

var $ = require('jquery'),
    Swiper = require('./utils/swiperUtil');

Swiper();

$('#toggleMenuButton').on('click', function(e) {
    e.preventDefault();
    $('#toggleMenuButton').toggleClass('header__menu-button--open-state');

    if($('#sidenavSection').width() === 0){
          $('.wrapper').css('height', '100vh');
          $('#sidenavSection').css('width', '100%');
    }
    else{
        $('.wrapper').css('height', 'auto');
          $('#sidenavSection').css('width', '0');
    }
  
});

$('#messageField').on('blur', function(e) {
    if ($('#messageField').val()) {
        $('.textarea-label').addClass('textfield__label--floatingLabel');
    } else {
        $('.textarea-label').removeClass('textfield__label--floatingLabel');
    }
});