var Swiper = require('swiper');

var activateSwiper = function(){
    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        nextButton: '.client-said__button-next',
        prevButton: '.client-said__button-prev',
        pagination: '.swiper-pagination',
        autoplay: 4000
    });
};


module.exports = activateSwiper;
